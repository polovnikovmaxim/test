# Veeam Test

This React application allows to render the UI for the received data from JSON file. [Veeam](https://veeam.herokuapp.com/)

## Installation

In order to use this application it is needed to have installed:
* Node.js >= 10.16
* npm >= 5.6
* yarn >= 1.1.0 (not necessary)


## Usage

Firstly it is needed to install all necessary libraries by running 
```bash
npm install 
```
or if you have yarn installed
```bash
yarn install
```

If you want to run the application on your local machine go to the project folder open the console and run 
```bash
npm build
npm start
``` 
or if you have yarn installed
```bash
yarn start_fe
``` 
After go to the [localhost:3000](http://localhost:3000/) in your preferred browser



## Possible extensions
This application could be extended by following features:
* Pagination
* Go to top arrow
* Toggled sidebar for filter menu on mobile view
