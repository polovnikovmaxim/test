import {initialProductState, ProductState} from "../states/ProductState";
import {ActionType, ProductActions} from "../actions/ProductActions";

export const productReducer = (state: ProductState = initialProductState, action: ProductActions) => {
  switch (action.type) {
    case ActionType.AddProduct:
      return {...state, products: [...state.products, action.payload]}
    case ActionType.AddVisibility:
      return {...state, visibility: [...state.visibility, action.payload]}
    case ActionType.RemoveProduct:
      return {...state, products: state.products.filter((product: string) => (
        product !== action.payload
        ))}
    case ActionType.RemoveVisibility:
      return {...state, visibility: state.visibility.filter((vis: string) => (
          vis !== action.payload
        ))}
    case ActionType.SortBy:
      return {...state, sort: state.sort === action.payload ? null : action.payload}
    case ActionType.PinProduct:
      return {...state, pined: action.payload}
    default:
      return state
  }
}