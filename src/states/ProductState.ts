import {Product} from "../interfaces/Product";

export interface ProductState {
  products: string[];
  visibility: string[];
  sort: string | null;
  pined: Product | null;
}

export const initialProductState: ProductState = {
  products: [],
  visibility: [],
  sort: null,
  pined: null
}