export interface Product {
  author: string;
  downloadLink: string;
  editFormLink: string;
  link: string;
  product: string;
  publicationDate: string;
  rank: number;
  shortDescription: string;
  title: string;
  visibility: string;
}

export interface FilterOption {
  optionName: string;
  options: string[];
}
