import React, {useContext, useEffect, useState} from 'react';
import {FilterOption, Product} from "../../interfaces/Product";
import "./dashboard.sass"
import {Sidebar} from "../../components/sidebar/Sidebar";
import {ProductsList} from "../../components/productsList/ProductsList";
import {ProductContext} from "../../context";
import moment from "moment";

interface DashboardProps {
  data: Product[]
}

export const Dashboard: React.FC<DashboardProps> = ({data}) => {

  const [filterOptions, setFilterOptions] = useState<FilterOption[]>([]);
  const [filteredData, setFilteredData] = useState<Product[]>(data)
  const [lastMonthUpdated, setLastMonthUpdated] = useState<number>(0)
  const {state} = useContext(ProductContext)


  // Collects options for filters & calculates number of products uploaded in last month
  useEffect(() => {

    setLastMonthUpdated(data.filter((product: Product) => (
      product.publicationDate > moment().subtract(1, 'months').toISOString()
    )).length)

    const collectFilterOptions = (entity: string) => {
      const temp: string[] = [];
      data.forEach((product: Product) => {

        // Check if product contains given entity
        if (entity in product) {
          if (temp.filter((option: string) => (
            // @ts-ignore
            option === product[entity]
          )).length < 1) {
            // @ts-ignore
            temp.push(product[entity])
          }
        }
      })
      return temp
    }

    const tempFilterOptions = [];

    tempFilterOptions.push({optionName: 'Product', options: collectFilterOptions('product')})
    tempFilterOptions.push({optionName: 'Visibility', options: collectFilterOptions('visibility')})

    setFilterOptions(tempFilterOptions);

  }, [data])


  // Applies filters
  useEffect(() => {
    const filters = {
      product: state.products,
      visibility: state.visibility
    }

    const filterData = () => {
      return data.filter((product: Product) => (
        Object.keys(filters).every(key => {
          // @ts-ignore
          if (!filters[key].length) return true;
          // @ts-ignore
          return filters[key].find(filter => product[key] === filter)
        })
      ))
    }

    const updatedData: Product[] = filterData()

    const temp: Product[] = data.length === updatedData.length ?
      data : state.pined ?
        updatedData.some(dat => dat === state.pined) ?
          updatedData : [...updatedData, state.pined] : updatedData

    setFilteredData(temp)

  }, [state.products, state.visibility, data, state.pined])


  return (
    <div className="main">
      <Sidebar filterOptions={filterOptions}/>
      <ProductsList products={filteredData} totalAmount={data.length} lastMonthUpdated={lastMonthUpdated}/>
    </div>
  )
}