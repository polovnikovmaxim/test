import React, {useContext} from 'react'
import "./Checkbox.sass"
import {ProductContext} from "../../context";
import {ActionType} from "../../actions/ProductActions";

interface CheckboxProps {
  optionName: string
  option: string
}

export const Checkbox: React.FC<CheckboxProps> = (
  {
    optionName,
    option
  }
) => {

  const {state, dispatch} = useContext(ProductContext)

  const checked = optionName === 'Product' ?
    state.products.filter((product: string) => (product === option)).length > 0 :
    state.visibility.filter((vis: string) => (vis === option)).length > 0

  const addFilter = () => {
    optionName === 'Product' ?
      checked ? dispatch({type: ActionType.RemoveProduct, payload: option}) :
        dispatch({type: ActionType.AddProduct, payload: option})
      :
      checked ? dispatch({type: ActionType.RemoveVisibility, payload: option}) :
        dispatch({type: ActionType.AddVisibility, payload: option})
  }

  return (
    <div className="checkbox">
      <input type="checkbox" checked={checked} onChange={addFilter} id={option}/>
      <label htmlFor={option}><span>{option}</span></label>
    </div>
  )
}