import React, {useReducer} from 'react';
import {Dashboard} from "./containers/dashboard/Dashboard";
import Data from 'data/data.json'
import {productReducer} from "./reducers/ProductReducer";
import {initialProductState} from "./states/ProductState";
import {ProductContext} from "./context";

const App = () => {

  const [state, dispatch] = useReducer(productReducer, initialProductState);

  return (
    <ProductContext.Provider value={{state, dispatch}}>
      <Dashboard data={Data.data}/>
    </ProductContext.Provider>
  )
}

export default App;
