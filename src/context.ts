import React from 'react'
import {initialProductState, ProductState} from "./states/ProductState";
import {ProductActions} from "./actions/ProductActions";

export const ProductContext = React.createContext<{
  state: ProductState;
  dispatch: React.Dispatch<ProductActions>;
}>({
  state: initialProductState,
  dispatch: () => undefined,
});