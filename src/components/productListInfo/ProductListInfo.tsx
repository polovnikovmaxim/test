import React, {useContext} from 'react'
import "./ProductListInfo.sass"
import {ProductContext} from "../../context";
import {ActionType} from "../../actions/ProductActions";

interface ProductListInfoProps {
  totalAmount: number
  lastMonthUploaded: number

}

export const ProductListInfo: React.FC<ProductListInfoProps> = (
  {
    totalAmount,
    lastMonthUploaded
  }
) => {

  const {state, dispatch} = useContext(ProductContext);

  const handleSort = (type: string) => {
    dispatch({type: ActionType.SortBy, payload: type})
  }

  return (
    <div className="headerMain">
      <div className="statistics">
        <span>Total amount: {totalAmount}</span>
        <span>Uploaded in last month: {lastMonthUploaded}</span>
      </div>
      <div className="sort">
        <span onClick={() => handleSort('Popular')} className={state.sort === 'Popular' ? 'selected' : ''}>Popular</span>
        <span>&nbsp;|&nbsp;</span>
        <span onClick={() => handleSort('Recent')} className={state.sort === 'Recent' ? 'selected' : ''}>Recent</span>
      </div>
    </div>
  )
}