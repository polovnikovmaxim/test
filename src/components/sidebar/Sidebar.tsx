import React from 'react'
import {FitlerOption} from "../filterOption/FilterOption";
import {FilterOption} from "../../interfaces/Product";
import "./Sidebar.sass"

interface SidebarProps {
  filterOptions: FilterOption[]
}

export const Sidebar: React.FC<SidebarProps> = (
  {filterOptions}
  ) => {

  return (
    <div className="sidebar">
      {filterOptions.map((option: FilterOption) => (
        <FitlerOption optionName={option.optionName} options={option.options} key={option.optionName}/>
      ))}
    </div>
  )
}