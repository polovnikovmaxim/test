import React from 'react'
import "./FilterOption.sass"
import {Checkbox} from "../../shared/Checkbox/Checkbox";

interface FilterOptionProps {
  optionName: string,
  options: string[]
}

export const FitlerOption: React.FC<FilterOptionProps> = (
  {
    optionName,
    options
  }
) => {

  return (
    <div className="box">
      <div className="header">
        {optionName}
      </div>
      <ul>
        {options.map((option: string) => (
          <li key={option}>
            <Checkbox option={option} optionName={optionName}/>
          </li>
        ))}
      </ul>
    </div>
  )
}