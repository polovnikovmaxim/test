import React, {useContext} from 'react'
import {Product} from "../../interfaces/Product";
import "./ProductInfo.sass"
import {FaDownload, FaEdit, FaLink} from "react-icons/fa";
import {ProductContext} from "../../context";
import {ActionType} from "../../actions/ProductActions";

interface ProductInfoProps {
  product: Product
}

export const ProductInfo: React.FC<ProductInfoProps> = ({product}) => {

  const {state, dispatch} = useContext(ProductContext)

  const handlePin = () => {
    dispatch({type: ActionType.PinProduct, payload: product === state.pined ? null : product})
  }


  return (
    <li className={`productContainer ${state.pined === product && 'pined'}`} onClick={handlePin}>
      <div className="product">
        <div className="productImage">
          <div className="imageBox">
            <span>{product.visibility}</span>
            <div className="greenBox"/>
          </div>
        </div>
        <div className="productInfo">
          <p className="title">{product.title}</p>
          <div className="productBody">
            <div className="descriptionActions">
              <p className="description">{product.shortDescription}</p>
              <div className="actions">
                <a href={product.link}><FaLink/>Copy</a>
                <a href={product.downloadLink}><FaDownload/>Download</a>
                <a href={product.editFormLink}><FaEdit/>Update</a>
              </div>
            </div>
            <div className="sideInfo">
              <span className="author">{product.author}</span>
              <span className="creationDate">{new Intl.DateTimeFormat('en-US', {
                year: 'numeric',
                month: 'short',
                day: '2-digit'
              }).format(Date.parse(product.publicationDate))}</span>
            </div>
          </div>
        </div>
      </div>
    </li>
  )
}