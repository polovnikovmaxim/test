import React, {useContext, useEffect, useState} from 'react'
import {Product} from "../../interfaces/Product";
import {ProductInfo} from "../productInfo/ProductInfo";
import "./ProductsList.sass"
import {ProductListInfo} from "../productListInfo/ProductListInfo";
import {ProductContext} from "../../context";

interface ProductsListProps {
  products: Product[];
  totalAmount: number;
  lastMonthUpdated: number;
}

export const ProductsList: React.FC<ProductsListProps> = (
  {
    products,
    totalAmount,
    lastMonthUpdated
  }
) => {

  const {state} = useContext(ProductContext)
  const [sorted, setSorted] = useState<Product[]>([])

  useEffect(() => {
    setSorted([...products])
  }, [products, state.sort, state.pined])

  const sort = (property: string | null) => {
    return sorted.sort((a, b) => {
      if (a === state.pined)
        return -1
      if (b === state.pined)
        return 1
      // @ts-ignore
      return property === null ? 1 : a[property] <= b[property] ? 1 : -1
    }).map((product: Product, i:number) => (
      // @ts-ignore
      <ProductInfo product={product} key={i}/>
    ))
  }

  return (
    <div className="productList">
      <ProductListInfo totalAmount={totalAmount} lastMonthUploaded={lastMonthUpdated}/>
      <ul>
        {
          state.sort === 'Popular' ?
            sort('rank') : state.sort === 'Recent' ?
            sort('publicationDate') : state.pined !== null ?
              sort(null) :
              products.map((product: Product) => (
                <ProductInfo product={product} key={product.rank}/>
              ))
        }
      </ul>
    </div>
  )
}