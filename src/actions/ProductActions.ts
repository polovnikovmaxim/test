import {Product} from "../interfaces/Product";

export enum ActionType {
  AddProduct,
  AddVisibility,
  RemoveProduct,
  RemoveVisibility,
  SortBy,
  PinProduct
}

export interface AddProduct {
  type: ActionType.AddProduct;
  payload: string;
}

export interface AddVisibility {
  type: ActionType.AddVisibility;
  payload: string;
}

export interface RemoveProduct {
  type: ActionType.RemoveProduct;
  payload: string;
}

export interface RemoveVisibility {
  type: ActionType.RemoveVisibility;
  payload: string;
}

export interface SortBy {
  type: ActionType.SortBy
  payload: string
}

export interface PinProduct {
  type: ActionType.PinProduct
  payload: Product | null
}

export type ProductActions =  AddProduct | AddVisibility | RemoveProduct | RemoveVisibility | SortBy | PinProduct
